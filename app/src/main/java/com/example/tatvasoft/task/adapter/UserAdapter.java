package com.example.tatvasoft.task.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tatvasoft.R;
import com.example.tatvasoft.databinding.RecyclerviewUsersBinding;
import com.example.tatvasoft.task.model.User;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserAdapter extends PagedListAdapter<User, UserAdapter.ItemViewHolder> {

    private Context mCtx;

    public UserAdapter(Context mCtx) {
        super(DIFF_CALLBACK);
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(RecyclerviewUsersBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        User item = getItem(position);

        if (item != null) {
            holder.recyclerviewUsersBinding.textViewName.setText(item.getName());
            Glide.with(mCtx)
                    .load(item.getImage())
                    .into(holder.recyclerviewUsersBinding.imageView);

            ItemAdapter itemAdapter = new ItemAdapter(mCtx, item.getItems());
            holder.recyclerviewUsersBinding.rvItem.setAdapter(itemAdapter);

            //set recycler view layout
            GridLayoutManager gridLayoutManager = new GridLayoutManager(mCtx, 2);
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (item.getItems().size() / 2 == 0) {
                        return 2;
                    } else {
                        return 1;
                    }
                }
            });
            holder.recyclerviewUsersBinding.rvItem.setLayoutManager(gridLayoutManager);
        } else {
            Toast.makeText(mCtx, "Item is null", Toast.LENGTH_LONG).show();
        }
    }

    private static DiffUtil.ItemCallback<User> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<User>() {
                @Override
                public boolean areItemsTheSame(User oldItem, User newItem) {
                    return oldItem.getName().equals(newItem.getName());
                }

                @Override
                public boolean areContentsTheSame(User oldItem, User newItem) {
                    return oldItem.equals(newItem);
                }
            };

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private RecyclerviewUsersBinding recyclerviewUsersBinding;

        public ItemViewHolder(RecyclerviewUsersBinding recyclerviewUsersBinding) {
            super(recyclerviewUsersBinding.getRoot());
            this.recyclerviewUsersBinding = recyclerviewUsersBinding;
        }
    }
}