package com.example.tatvasoft.network;

import com.example.tatvasoft.task.model.UserDetail;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

    @GET("users")
    Call<UserDetail> getUsers(@Query("offset") int offset, @Query("limit") int limit);
}